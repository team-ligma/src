#include "main.h"

#define LOW1	GPIO_PIN_2
#define LOW2	GPIO_PIN_7
#define LOW3	GPIO_PIN_1
#define LOW4	GPIO_PIN_3
#define MID1	GPIO_PIN_8
#define MID2	GPIO_PIN_11
#define MID3	GPIO_PIN_5
#define MID4	GPIO_PIN_4
#define HIGH1	GPIO_PIN_9
#define HIGH2	GPIO_PIN_10
#define HIGH3	GPIO_PIN_6
#define HIGH4	GPIO_PIN_1

ADC_HandleTypeDef hadc1;
UART_HandleTypeDef huart2;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART2_UART_Init(void);
void delay(int Num);

int main(void)
{
	uint16_t raw;
	int SpectrumState = 0;

	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_ADC1_Init();

	while (1)
	{
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4))
		{
			if(SpectrumState == 0)
			{
				SpectrumState = 1;
			}
			else if(SpectrumState == 1)
			{
				SpectrumState = 0;
			}
			delay(1);
		}

		HAL_Delay(100);

		//Get ADC value
		HAL_ADC_Start(&hadc1);
		HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
		raw = HAL_ADC_GetValue(&hadc1);

		if(SpectrumState == 0) //Bar Setting
		{
			//Level 1 Volume
			if(raw > 220)
			{
				HAL_GPIO_WritePin(GPIOA, LOW1, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, MID1, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, HIGH1, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LOW1, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, MID1, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, HIGH1, GPIO_PIN_RESET);
			}

			//Level 2 Volume
			if(raw > 1023)
			{
				HAL_GPIO_WritePin(GPIOA, LOW2, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, MID2, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, HIGH2, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LOW2, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, MID2, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, HIGH2, GPIO_PIN_RESET);
			}

			//Level 3 Volume
			if(raw > 2046)
			{
				HAL_GPIO_WritePin(GPIOA, LOW3, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, MID3, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, HIGH3, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LOW3, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, MID3, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, HIGH3, GPIO_PIN_RESET);
			}

			//Level 4 Volume
			if(raw > 3072)
			{
				HAL_GPIO_WritePin(GPIOB, LOW4, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, MID4, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, HIGH4, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOB, LOW4, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, MID4, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, HIGH4, GPIO_PIN_RESET);
			}
		}
		else //Floating Setting
		{
			//Level 1 Volume
			if(raw > 220 && raw <= 1022)
			{
				HAL_GPIO_WritePin(GPIOA, LOW1, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, MID1, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, HIGH1, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LOW1, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, MID1, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, HIGH1, GPIO_PIN_RESET);
			}

			///Level 2 Volume
			if(raw > 1023 && raw <= 2046)
			{
				HAL_GPIO_WritePin(GPIOA, LOW2, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, MID2, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, HIGH2, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LOW2, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, MID2, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOA, HIGH2, GPIO_PIN_RESET);
			}

			//Level 3 Volume
			if(raw > 2046 && raw <= 3072)
			{
				HAL_GPIO_WritePin(GPIOA, LOW3, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, MID3, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, HIGH3, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOA, LOW3, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, MID3, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, HIGH3, GPIO_PIN_RESET);
			}

			//Level 4 Volume
			if(raw > 3072)
			{
				HAL_GPIO_WritePin(GPIOB, LOW4, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, MID4, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOB, HIGH4, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(GPIOB, LOW4, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, MID4, GPIO_PIN_RESET);
				HAL_GPIO_WritePin(GPIOB, HIGH4, GPIO_PIN_RESET);
			}
		}
		HAL_Delay(1);
	}
}

void delay(int Num)
{
	for(int x = 0; x < Num*100000; x++) {}
}

void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

static void MX_ADC1_Init(void)
{
  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
}

static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_7|GPIO_PIN_8
                          |GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA1 PA2 PA7 PA8
                           PA9 PA10 PA11 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_7|GPIO_PIN_8
                          |GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB1 PB3 PB4 PB5
                           PB6 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1) {}
}

#ifdef  USE_FULL_ASSERT
void assert_failed(uint8_t *file, uint32_t line) {}
#endif /* USE_FULL_ASSERT */